{{ define "VSphereMachineTemplateSpec-CP" }}
cloneMode: {{ .Values.capv.template_clone_mode }}
datacenter: {{ .Values.capv.dataCenter | quote }}
{{- if .Values.capv.dataStore }}
datastore: {{ .Values.capv.dataStore }}
{{- end }}
diskGiB: {{ .Values.capv.control_plane_profile.diskGiB }}
folder: {{ .Values.capv.folder }}
memoryMiB: {{ .Values.capv.control_plane_profile.memoryMiB }}
network:
  devices:
  {{- range $network_key, $network_def := .Values.capv.networks }}
    - dhcp4: {{ $network_def.dhcp4 | default false }}
      networkName: {{ $network_def.networkName }}
  {{- end }}
numCPUs: {{ .Values.capv.control_plane_profile.numCPUs }}
resourcePool: {{ .Values.capv.resourcePool }}
server: {{ .Values.capv.server }}
{{- if .Values.capv.storagePolicyName }}
storagePolicyName: {{ .Values.capv.storagePolicyName }}
{{- end }}
template: {{ .Values.image }}
thumbprint: {{ .Values.capv.tlsThumbprint }}
{{ end }}
